# Symfony CKEditor 5 Webpack Vite Bundle

This bundle simplifies the integration of CKEditor 5 into Symfony applications, offering various configuration options
for different form fields. It includes a wide range of free tools to enhance the functionality of your text editors.

[![Pipeline Status](https://gitlab.com/daddl3/symfony-ckeditor-5-webpack-vite/badges/1.1.2/pipeline.svg)]()

[![Version](https://img.shields.io/badge/version-1.1.2-blue)](https://gitlab.com/daddl3/symfony-ckeditor-5-webpack-vite/blob/master/version.txt)

## Installation

To install this bundle, execute the following command in your project directory:

```bash
composer require daddl3/symfony-ckeditor-5-webpack
```

This command adds the Symfony CKEditor 5 Webpack Vite bundle to your project dependencies.

#### Register

Register the bundle into your symfony project

```php
# ./config/bundles.php
<?php

return [
    daddl3\SymfonyCKEditor5WebpackViteBundle\SymfonyCKEditor5WebpackViteBundle::class => ['all' => true],
];
```

### [See how to handle it](./docs/usage.md)

<br>
<br>

Please don't hesitate to report bugs!

Note that I have only tested the “standard plugins” at the moment.
