# Changelog

## Version 1.2.0

-   add BalloonToolbar and BlockToolbar
-   enable all editors
    -   Balloon editor
    -   Document/Decoupled Editor
    -   Inline Editor
    -   MultiLevel Editor

## Version 1.1.2

-   Error Fix with multiple editors on one side.
-   Add better logs to js

## Version 1.1.1

-   default config is present in english

## Version 1.1.0

-   Updated to CKEditor 5 version 43.0.0 with new plugin mergeFields.
-   Introduced dark mode with toggle functionality.

## Version 1.0.1

-   Fixed regex issue when using the default configuration.
-

## Version 1.0.0

-   Initial release of the first version.
-   CKEditor 5 version 24.0.0 with all standard and premium plugins.
