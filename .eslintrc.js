module.exports = {
    env: {
        node: true
    },
    extends: [
        "eslint:recommended",
        "plugin:vue/vue3-strongly-recommended",
        "plugin:vue/vue3-recommended",
        "prettier"
    ],
    plugins: ["prettier"],
    rules: {
        // override/add rules settings here, such as:
        "vue/no-unused-vars": "error",
        "no-multiple-empty-lines": "error",
        "space-before-blocks": "error",
        "no-unreachable": "error",
        "no-irregular-whitespace": "error",
        "eslint-plugin-babel": "error",
        "prettier/prettier": "error"
    }
};
