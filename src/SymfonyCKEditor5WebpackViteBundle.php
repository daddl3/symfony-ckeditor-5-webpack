<?php

namespace daddl3\SymfonyCKEditor5WebpackViteBundle;

use daddl3\SymfonyCKEditor5WebpackViteBundle\Form\Ckeditor5TextareaType;
use RuntimeException;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class SymfonyCKEditor5WebpackViteBundle extends AbstractBundle
{
    protected string $extensionAlias = 'symfony_ckeditor5';

    public function configure(DefinitionConfigurator $definition): void
    {
        /** @var ArrayNodeDefinition $nodeDefinition */
        $nodeDefinition = $definition->rootNode();
        // @formatter:off
        $nodeDefinition // Start of root node configuration
        ->children()
            ->arrayNode('editors') // Start of 'editors' configuration
                ->useAttributeAsKey('name')
                ->arrayPrototype() // Use arrayPrototype to allow any structure under each editor
                    ->variablePrototype() // Allows any type of value, enabling users to define their own configurations freely
                    ->end()
                ->end()
            ->end()
            ->booleanNode('darkmode') // Adding 'darkmode' as a boolean option on the same level as 'editors'
                ->defaultNull()
                ->end()
            ->end();
        // @formatter:on
    }

    public function prependExtension(ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $bundles = $builder->getParameter('kernel.bundles');

        if (!isset($bundles['TwigBundle'])) {
            throw new RuntimeException('TwigBundle is not loaded. Please make sure TwigBundle is installed and enabled. You can install it by running: composer require symfony/twig-bundle');
        }

        // Konfiguriere Twig mit deinem Form-Theme
        $builder->prependExtensionConfig('twig', [
            'form_theme' => [
                '@SymfonyCKEditor5WebpackVite/form_theme.html.twig',
            ],
        ]);
    }

    /**
     * @param array<mixed> $config
     */
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import(__DIR__.'/../config/service.yaml');

        // Here you can load services or other configurations based on the $config
        // For example, you can set the configuration as a parameter to use it later in your services
        $builder->setParameter('symfony_ckeditor5.config', $config);

        $definition = new Definition(
            Ckeditor5TextareaType::class,
            [ // Constructor arguments for Ckeditor5TextareaType
                '%symfony_ckeditor5.config%', // Inject the 'editors' configurations
            ]
        );

        $builder
            ->setDefinition('form.symfony_ckeditor5_textarea', $definition)
            ->addTag('form.type')
            ->setAutowired(true)
            ->setAutoconfigured(true)
            ->setPublic(false);
    }
}
