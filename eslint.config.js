module.exports = {
    files: ["js/**/*.js", "ts/**/*.ts"], // Adding TypeScript files
    linterOptions: {
        reportUnusedDisableDirectives: true
    },
    parser: "@typescript-eslint/parser", // Add TypeScript parser
    plugins: {
        prettier: {
            rules: {
                "prettier/prettier": [
                    "error",
                    {
                        printWidth: 90,
                        useTabs: false,
                        semi: true,
                        trailingComma: "none",
                        bracketSpacing: true,
                        arrowParens: "always",
                        htmlWhitespaceSensitivity: "ignore",
                        singleQuote: false,
                        tabWidth: 4,
                        "at-rule-no-vendor-prefix": true,
                        "media-feature-name-no-vendor-prefix": true,
                        "property-no-vendor-prefix": true,
                        "selector-no-vendor-prefix": true,
                        "value-no-vendor-prefix": true,
                        "color-hex-case": "lower",
                        "max-empty-lines": 1,
                        "rule-empty-line-before": "always",
                        "block-opening-brace-space-before": "always",
                        "no-descending-specificity": null
                    }
                ]
            }
        },
        "@typescript-eslint": {
            rules: {
                "@typescript-eslint/no-unused-vars": ["error"],
                "@typescript-eslint/explicit-function-return-type": ["warn"]
            }
        }
    },
    rules: {
        prettier: "error",
        "no-unused-vars": "error",
        "no-multiple-empty-lines": [
            "error",
            {
                max: 2,
                maxEOF: 1,
                maxBOF: 0
            }
        ],
        "space-before-blocks": "error",
        "no-unreachable": "error",
        "no-irregular-whitespace": "error"
        // Add Vue and other plugin-specific rules here as needed
    }
};
