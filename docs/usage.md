[Back to Overview](../README.md)

## Usage

### General

To customize a configuration, simply include a YAML file within your package as described in the following steps. This setup allows you to define multiple editors, each with its own configuration.

The configuration is designed to be flexible, enabling you to specify different configurations for various editors as needed.

Note that not all plugins are loaded in the browser by default; they are only loaded when needed. For more details, you can refer to [symfony_ckeditor5.yaml](https://gitlab.com/daddl3/symfony-ckeditor-5-webpack-vite/-/blob/main/config/symfony_ckeditor5.yaml?ref_type=heads), which serves as an example of how to use it.

The entire code is written asynchronously to ensure that the code is split into smaller chunks. This approach prevents the entire CKEditor code from being bundled into one large file, significantly improving load times and reducing the size of individual files.

This Bundle works with CkEditor5 Version `43.0.0`

### Adding to `package.json`

To ensure the necessary packages are installed, include the following lines in your `package.json` file within the
dependencies section:

```json
"dependencies": {
  "@ckeditor/ckeditor5-theme-lark": "^43.0.0",
  "ckeditor5": "^43.0.0",
  "ckeditor5-premium-features": "^43.0.0"
},
```

Then, run `npm install` or `yarn install` to install all required packages.

### Overwrite Default Configuration

If you want to overwrite the default configuration, you have to set the configuration in the YAML file like this:

```yaml
# config/packages/symfony_ckeditor5.yaml
symfony_ckeditor5:
    editors:
        default:
            toolbar:
                items:
```

### Dark Mode

The theme is automatically selected based on the system settings, either dark or light mode.
You can toggle it with a boolean like this.

```yaml
# config/packages/symfony_ckeditor5.yaml
symfony_ckeditor5:
    darkmode: true
```

### [Different editors](./usage/editors.md)

### [A list for integrated Plugins](./usage/html_features.md)

### [HTML Features](./usage/plugins.md)

### [Webpack Integration](./usage/webpack.md)

### [Vite Integration](./usage/vite.md)

### [Form Integration](./usage/form.md)

### [Image upload](./usage/image.md)

<br>
<br>

[Back to Overview](../README.md)
