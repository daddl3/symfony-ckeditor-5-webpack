## Available Editors

`At Version 43.0.0`

I have given you the option to use the different editors.
With a little configuration, this is then implemented automatically.
To make sure that everything works, please read the docs that I have linked.

#### Normal Editors

-   Ballon Editor `Balloon` [Balloon editor](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/balloon-editor.html)
-   Document Editor `Decoupled` [Decoupled editor](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/document-editor.html)
-   Inline Editor `Inline` [Inline editor](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/inline-editor.html)

Just add `editor` to your configuration.

```yaml
# config/packages/symfony_ckeditor5.yaml
symfony_ckeditor5:
    editors:
        custom1:
            editor: "Decouple"
            toolbar:
```

#### Specific Editors

-   Multi Root Editor `Multi` [Multi-root editor](https://ckeditor.com/docs/ckeditor5/latest/examples/builds/multi-root-editor.html)

To use the Multi Root Editor you have to define the `multiEditorClass` in your configuration.
Then add the class to every textarea you want to use the Multi Root Editor.

Don't forget to add the class also on the Ckeditor5TextareaType.

```yaml
# config/packages/symfony_ckeditor5.yaml
symfony_ckeditor5:
    editors:
        custom1:
            editor: "Multi"
            multiEditorClass: "ck-multi-editor"
            toolbar:
```

```php
->add('form', TextareaType::class, [
    'attr' => [
        'data-ckeditor5-config' => 'custom'
        'class' => 'ck-multi-editor'
    ],
])
```

<br>
<br>

[Back to Usage](../usage.md)
