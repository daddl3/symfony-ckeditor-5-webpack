[Back to Usage](../usage.md)

### Form Integration

```php
->add('form', Ckeditor5TextareaType::class, [
    'attr' => [
        'data-ckeditor5-config' => 'custom'
    ],
])
```

Add your configuration in the config.
You can also leave out the attribute, and a standard configuration will then be automatically adopted. If you store a
config that is called "default", then this standard configuration will be used.

### Easy Admin

To use this form, you have to implement it:

```yaml
easy_admin:
    entities:
        Ckeditor5TextareaType: # Replace with your actual entity name
            class: daddl3\SymfonyCKEditor5WebpackViteBundle\Form\Ckeditor5TextareaType
            form:
                fields:
                    # Define other fields as needed
                    - { property: "text", type: 'App\Form\Ckeditor5TextareaType' }
```

And add it to your form:

```php
TextareaField::new('body')
    ->setFormType(Ckeditor5TextareaType::class)
    // If you want to use your own config
    ->setFormTypeOptions([
        'attr' => [
            'data-ckeditor5-config' => 'custom1'
        ],
    ]),
    // Don't forget to implement the JavaScript and CSS
    ->addHtmlContentsToHead($script . $link),
```

<br>
<br>

[Back to Usage](../usage.md)
