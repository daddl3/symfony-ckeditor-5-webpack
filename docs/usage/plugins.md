## Available Plugins

`Version 42.0.0`

**Always installed plugins:**

-   Essentials `essentials` [CKEditor Essentials](https://ckeditor.com/docs/ckeditor5/latest/api/essentials.html)
-   Autoformat `autoformat` [CKEditor Autoformat](https://ckeditor.com/docs/ckeditor5/latest/features/autoformat.html)
-   Texttransformation `texttransformation` [CKEditor Text Transformation](https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html)
-   Paragraph `paragraph` [CKEditor Paragraph](https://ckeditor.com/docs/ckeditor5/latest/api/paragraph.html)

**Standard plugins (loaded via Toolbar Items):**

-   AccessibilityHelp `accessibilityHelp` [CKEditor Accessibility Help](https://ckeditor.com/docs/ckeditor5/latest/features/accessibility.html)
-   Alignment `alignment` [CKEditor Alignment](https://ckeditor.com/docs/ckeditor5/latest/features/text-alignment.html)
-   AutoLink `autoLink` [CKEditor Auto Link](https://ckeditor.com/docs/ckeditor5/latest/features/link.html)
-   BlockQuote `blockQuote` [CKEditor Block Quote](https://ckeditor.com/docs/ckeditor5/latest/features/block-quote.html)
-   Bold `bold` [CKEditor Bold](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)
-   CloudServices `cloudServices` [CKEditor Cloud Services](https://ckeditor.com/docs/cs/latest/guides/overview.html)
-   Code `code`
-   CodeBlock `codeBlock` [CKEditor Code Block](https://ckeditor.com/docs/ckeditor5/latest/features/code-blocks.html)
-   ExportPdf `exportPdf` [CKEditor Export to PDF](https://ckeditor.com/docs/ckeditor5/latest/features/converters/export-pdf.html)
-   ExportWord `exportWord` [CKEditor Export to Word](https://ckeditor.com/docs/ckeditor5/latest/features/converters/export-word.html)
-   FindAndReplace `findAndReplace` [CKEditor Find and Replace](https://ckeditor.com/docs/ckeditor5/latest/features/find-and-replace.html)
-   FontBackgroundColor `fontBackgroundColor` [CKEditor Font Background Color](https://ckeditor.com/docs/ckeditor5/latest/features/font.html#font-background-color)
-   FontColor `fontColor` [CKEditor Font Color](https://ckeditor.com/docs/ckeditor5/latest/features/font.html#font-color)
-   FontFamily `fontFamily` [CKEditor Font Family](https://ckeditor.com/docs/ckeditor5/latest/features/font.html#font-family)
-   FontSize `fontSize` [CKEditor Font Size](https://ckeditor.com/docs/ckeditor5/latest/features/font.html#font-size)
-   GeneralHtmlSupport `generalHtmlSupport` [CKEditor General HTML Support](https://ckeditor.com/docs/ckeditor5/latest/features/general-html-support.html)
-   FullPage `fullPage` [CKEditor Full Page](https://ckeditor.com/docs/ckeditor5/latest/features/html/full-page-html.html)
-   Heading `heading` [CKEditor Heading](https://ckeditor.com/docs/ckeditor5/latest/features/headings.html)
-   Highlight `highlight` [CKEditor Highlight](https://ckeditor.com/docs/ckeditor5/latest/features/highlight.html)
-   HorizontalLine `horizontalLine` [CKEditor Horizontal Line](https://ckeditor.com/docs/ckeditor5/latest/features/horizontal-line.html)
-   HtmlEmbed `htmlEmbed` [CKEditor HTML Embed](https://ckeditor.com/docs/ckeditor5/latest/features/html-embed.html)
    -   when set `generalHtmlSupport` load automatically
-   Image [CKEditor Image](https://ckeditor.com/docs/ckeditor5/latest/features/image.html)
-   ImageCaption [CKEditor Image Caption](https://ckeditor.com/docs/ckeditor5/latest/features/images/images-captions.html)
-   ImageEditing [CKEditor Image Editing](https://ckeditor.com/docs/ckeditor5/latest/api/module_image_image_imageediting-ImageEditing.html)
-   ImageResize [CKEditor Image Resize](https://ckeditor.com/docs/ckeditor5/latest/features/images/images-resizing.html)
-   InsertImage `insertImage` [CKEditor Insert Image](https://ckeditor.com/docs/ckeditor5/latest/features/images/images-inserting.html)
    -   when set `image`, `imageEditing`, `imageBlock`, `imageCaption`, `imageStyle`, `imageToolbar`, `imageResize`, `cloudServices` load automatically
-   ImageStyle [CKEditor Image Style](https://ckeditor.com/docs/ckeditor5/latest/features/images/images-styles.html)
-   ImageUpload `imageUpload` [CKEditor Image Upload](https://ckeditor.com/docs/ckeditor5/latest/features/images/image-upload/image-upload.html)
    -   when set `image`, `imageEditing`, `imageBlock`, `imageCaption`, `imageStyle`, `imageToolbar`, `imageResize`, `cloudServices` load automatically
-   Indent `indent` [CKEditor Indent](https://ckeditor.com/docs/ckeditor5/latest/features/indent.html)
-   Italic `italic` [CKEditor Italic](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)
-   Link `link` [CKEditor Link](https://ckeditor.com/docs/ckeditor5/latest/features/link.html)
-   List [CKEditor List](https://ckeditor.com/docs/ckeditor5/latest/features/lists.html)
-   MediaEmbed `mediaEmbed` [CKEditor Media Embed](https://ckeditor.com/docs/ckeditor5/latest/features/media-embed.html)
-   PageBreak `pageBreak` [CKEditor Page Break](https://ckeditor.com/docs/ckeditor5/latest/features/page-break.html)
-   Paragraph [CKEditor Paragraph](https://ckeditor.com/docs/ckeditor5/latest/api/paragraph.html)
-   PasteFromOffice [CKEditor Paste from Office](https://ckeditor.com/docs/ckeditor5/latest/features/pasting/paste-from-office.html)
-   RemoveFormat `removeFormat` [CKEditor Remove Format](https://ckeditor.com/docs/ckeditor5/latest/features/remove-format.html)
-   RestrictedEditingException `restrictedEditingException` [CKEditor Restricted Editing Exception](https://ckeditor.com/docs/ckeditor5/latest/features/restricted-editing.html)
-   RestrictedEditing `restrictedEditing` [CKEditor Restricted Editing](https://ckeditor.com/docs/ckeditor5/latest/features/restricted-editing.html)
-   SelectAll `selectAll` [CKEditor Select All](https://ckeditor.com/docs/ckeditor5/latest/features/select-all.html)
-   ShowBlocks `showBlocks` [CKEditor Show Blocks](https://ckeditor.com/docs/ckeditor5/latest/features/show-blocks.html)
-   SourceEditing `sourceEditing` [CKEditor Source Editing](https://ckeditor.com/docs/ckeditor5/latest/features/source-editing.html)
-   SpecialCharacters `specialCharacters` [CKEditor Special Characters](https://ckeditor.com/docs/ckeditor5/latest/features/special-characters.html)
-   SpecialCharactersEssentials [CKEditor Special Characters Essentials](https://ckeditor.com/docs/ckeditor5/latest/features/special-characters.html#special-characters-essential)
-   Strikethrough `strikethrough` [CKEditor Strikethrough](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)
-   Subscript `subscript` [CKEditor Subscript](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)
-   Superscript `superscript` [CKEditor Superscript](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)
-   Style `style` [CKEditor Style](https://ckeditor.com/docs/ckeditor5/latest/features/style.html)
-   Table [CKEditor Table](https://ckeditor.com/docs/ckeditor5/latest/features/tables/tables.html)
    -   when set `tableToolbar`, `tableProperties`, `tableCellProperties`, `tableColumnResize`, `tableCaption` load automatically
-   TextPartLanguage `textPartLanguage` [CKEditor Text Part Language](https://ckeditor.com/docs/ckeditor5/latest/features/language.html)
-   TextPartLanguageEditing [CKEditor Text Part Language Editing](https://ckeditor.com/docs/ckeditor5/latest/features/language.html)
-   Texttransformation [CKEditor Text Transformation](https://ckeditor.com/docs/ckeditor5/latest/features/text-transformation.html)
-   TodoList `todoList` [CKEditor Todo List](https://ckeditor.com/docs/ckeditor5/latest/features/lists/todo-lists.html)
-   Underline `underline` [CKEditor Underline](https://ckeditor.com/docs/ckeditor5/latest/features/basic-styles.html)

**Premium plugins (loaded via Toolbar Items):**

-   MultiLevelList `multiLevelList` [CKEditor Multi-Level List](https://ckeditor.com/docs/ckeditor5/latest/features/lists/multi-level-lists.html)
-   AiAssistant `aiAssistant` [CKEditor AI Assistant](https://ckeditor.com/docs/ckeditor5/latest/features/ai-assistant.html)
-   OpenAITextAdapter `openAITextAdapter` [CKEditor OpenAI Text Adapter](https://ckeditor.com/docs/ckeditor5/latest/features/ai-assistant.html)
-   InsertTemplate `insertTemplate` [CKEditor Insert Template](https://ckeditor.com/docs/ckeditor5/latest/features/template.html)
-   FormatPainter `formatPainter` [CKEditor Format Painter](https://ckeditor.com/docs/ckeditor5/latest/features/format-painter.html)
-   CaseChange `caseChange` [CKEditor Case Change](https://ckeditor.com/docs/ckeditor5/latest/features/case-change.html)
-   TableOfContents `tableOfContents` [CKEditor Table of Contents](https://ckeditor.com/docs/ckeditor5/latest/features/table-of-contents.html)
-   PictureEditing `pictureEditing` [CKEditor Picture Editing](https://ckeditor.com/docs/ckeditor5/latest/api/module_image_pictureediting-PictureEditing.html)

**Plugins loaded via Configuration (loaded when you add a config for that Plugin):**
<br>
`just add an empty object when you just want to load them`

-   Mention `mention` [CKEditor Mention](https://ckeditor.com/docs/ckeditor5/latest/features/mentions.html)
-   SlashCommand `slashCommand` [CKEditor Slash Command](https://ckeditor.com/docs/ckeditor5/latest/features/slash-commands.html)
-   WordCount `wordCount` [CKEditor Word Count](https://ckeditor.com/docs/ckeditor5/latest/features/word-count.html) (not working yet)
-   Autosave `autosave` [CKEditor Autosave](https://ckeditor.com/docs/ckeditor5/latest/features/autosave.html)
-   Minimap `minimap` [CKEditor Minimap](https://ckeditor.com/docs/ckeditor5/latest/features/minimap.html)
-   DocumentOutline `documentOutline` [CKEditor Document Outline](https://ckeditor.com/docs/ckeditor5/latest/features/document-outline.html)
-   Title `title` [CKEditor Title](https://ckeditor.com/docs/ckeditor5/latest/features/title.html)
-   Clipboard `clipboard` [CKEditor Clipboard](https://ckeditor.com/docs/ckeditor5/latest/framework/deep-dive/clipboard.html) (not working yet)
-   Pagination `pagination` [CKEditor Pagination](https://ckeditor.com/docs/ckeditor5/latest/features/pagination/pagination.html)

**Plugins that have no configuration with item name**

-   AutoImage `autoimage` [CKEditor Auto Image](https://ckeditor.com/docs/ckeditor5/latest/features/images/images-inserting.html#installation-2)
-   HtmlComment `htmlcomment` [CKEditor HTML Comment](https://ckeditor.com/docs/ckeditor5/latest/features/html/html-comments.html)
-   Markdown `markdown` [CKEditor Markdown](https://ckeditor.com/docs/ckeditor5/latest/features/markdown.html)
-   Paste From Markdown Experimental `pasteFromMarkdownExperimental` [CKEditor Paste from Markdown Experimental](https://ckeditor.com/docs/ckeditor5/latest/features/markdown.html)
-   Paste From Office `pasteFromOffice` [CKEditor Paste from Office](https://ckeditor.com/docs/ckeditor5/latest/features/pasting/paste-from-office.html)
-   Paste From Office Enhanced(Premium Feature) `pastefromofficeenhanced` [CKEditor Paste from Office Enhanced](https://ckeditor.com/docs/ckeditor5/latest/features/pasting/paste-from-office.html)

you can use them with the extraPlugins entry.
It will be removed before initialization

`Version 43.0.0`

**Premium plugins (loaded via Toolbar Items):**

-   Merge fields `mergeFields` [CKEditor Merge fields List](https://ckeditor.com/docs/ckeditor5/latest/features/merge-fields.html)

```yaml
# config/packages/symfony_ckeditor5.yaml
symfony_ckeditor5:
    editors:
        custom1:
            toolbar:
                items:
                    - "sourceEditing"
                    - "|"
                    - "undo"
                    - "redo"
                    - "|"
                    - "heading"
                    - "|"
                    - "bold"
                    - "italic"
                    - "fontColor"
            extraPlugins:
                - "autoimage"
                - "markdown"
```

<br>
<br>

[Back to Usage](../usage.md)
