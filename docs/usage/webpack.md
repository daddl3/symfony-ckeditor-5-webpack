[Back to Usage](../usage.md)

### Webpack Configuration

For projects using Webpack, you need to integrate CKEditor with the following steps:

1. Require the CKEditor webpack entry file in your Webpack configuration:

```js
const addCKEditor = require("./vendor/daddl3/symfony-ckeditor-5-webpack/assets/js/ckeditor-webpack-entry");
```

2. Invoke the `addCKEditor` function passing in the Encore object:

```js
/** Encore, sourceMap **/
addCKEditor(Encore, true);
```

3. Add an entry for CKEditor in the Encore configuration:

```js
Encore.addEntry(
    "ckeditor5",
    "./vendor/daddl3/symfony-ckeditor-5-webpack/assets/js/ckeditor5.js"
);
```

4. Install `@ckeditor/ckeditor5-dev-utils`:

```bash
yarn add @ckeditor/ckeditor5-dev-utils --dev
```

5. Remove the css order warnings
   If you find a better solution, please share it with me

```js
.configureSplitChunks(splitChunks => {
    splitChunks.chunks = 'all';
    splitChunks.name = false;
    splitChunks.cacheGroups = {
        styles: {
            name: false,
            test: /\.css$/,
            chunks: 'all',
            enforce: true,
            }
        };
    })
```

<br>
<br>

[Back to Usage](../usage.md)
