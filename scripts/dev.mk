php-cs-fixer:
	@make toolkit.php-cs-fixer

rector:
	@docker run \
          		--rm \
          		-v ${PWD}:/srv/codebase/codebase \
          		-v ${PWD}/ci:/ci \
          		-e APP_DIR=/srv/codebase \
          		daddl3/toolkit rector

phpstan:
ifeq (, $(shell which docker))
	/srv/app/bin/console cache:warmup
	/tools/phpstan/vendor/bin/phpstan clear-result-cache
	/tools/phpstan/vendor/bin/phpstan analyse --memory-limit=2G -c /srv/phpstan.neon
else
	@make toolkit.phpstan
endif