# Include this file to every project to get started!
# This file should be located in <Project Root>/scripts/toolkit.mk

THIS_DIR:=$(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
THIS_MAKEFILE := $(abspath $(lastword $(MAKEFILE_LIST)))

toolkit.run/%:
	@APP_DIR=${APP_DIR:-app}
	@docker run \
		--rm \
		-v ${PWD}:/srv/codebase \
		-v ${PWD}/ci:/ci \
		-e APP_DIR=/srv/codebase/${APP_DIR} \
		daddl3/toolkit $*

toolkit.phpstan:
	@make toolkit.run/"phpstan"

toolkit.php-cs-fixer:
	@make toolkit.run/"php-cs-fixer"
