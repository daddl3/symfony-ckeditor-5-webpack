import { CustomUploadAdapter } from "../custom-upload-adapter";

type PluginImports = {
    [key: string]: string;
};

const pluginImports: PluginImports = {
    // Standard Plugins
    accessibilityHelp: "@ckeditor/ckeditor5-accessibility-help/src/accessibilityhelp",
    alignment: "@ckeditor/ckeditor5-alignment/src/alignment",
    autoformat: "@ckeditor/ckeditor5-autoformat/src/autoformat",
    autolink: "@ckeditor/ckeditor5-link/src/autolink",
    autosave: "@ckeditor/ckeditor5-autosave/src/autosave",
    blockQuote: "@ckeditor/ckeditor5-block-quote/src/blockquote",
    bold: "@ckeditor/ckeditor5-basic-styles/src/bold",
    code: "@ckeditor/ckeditor5-basic-styles/src/code",
    codeBlock: "@ckeditor/ckeditor5-code-block/src/codeblock",
    essentials: "@ckeditor/ckeditor5-essentials/src/essentials",
    exportPdf: "@ckeditor/ckeditor5-export-pdf/src/exportpdf",
    exportWord: "@ckeditor/ckeditor5-export-word/src/exportword",
    findAndReplace: "@ckeditor/ckeditor5-find-and-replace/src/findandreplace",
    fontBackgroundColor: "@ckeditor/ckeditor5-font/src/fontbackgroundcolor",
    fontColor: "@ckeditor/ckeditor5-font/src/fontcolor",
    fontFamily: "@ckeditor/ckeditor5-font/src/fontfamily",
    fontSize: "@ckeditor/ckeditor5-font/src/fontsize",
    generalHtmlSupport: "@ckeditor/ckeditor5-html-support/src/generalhtmlsupport",
    fullPage: "@ckeditor/ckeditor5-html-support/src/fullpage",
    heading: "@ckeditor/ckeditor5-heading/src/heading",
    highlight: "@ckeditor/ckeditor5-highlight/src/highlight",
    horizontalLine: "@ckeditor/ckeditor5-horizontal-line/src/horizontalline",
    htmlEmbed: "@ckeditor/ckeditor5-html-embed/src/htmlembed",
    image: "@ckeditor/ckeditor5-image/src/image",
    imageCaption: "@ckeditor/ckeditor5-image/src/imagecaption",
    imageEditing: "@ckeditor/ckeditor5-image/image/src/imageediting",
    imageBlock: "@ckeditor/ckeditor5-image/src/imageblock",
    imageResize: "@ckeditor/ckeditor5-image/src/imageresize",
    imageStyle: "@ckeditor/ckeditor5-image/src/imagestyle",
    imageToolbar: "@ckeditor/ckeditor5-image/src/imagetoolbar",
    imageUpload: "@ckeditor/ckeditor5-image/src/imageupload",
    indent: "@ckeditor/ckeditor5-indent/src/indent",
    italic: "@ckeditor/ckeditor5-basic-styles/src/italic",
    link: "@ckeditor/ckeditor5-link/src/link",
    list: "@ckeditor/ckeditor5-list/src/list",
    listProperties: "@ckeditor/ckeditor5-list/src/listproperties",
    markdown: "@ckeditor/ckeditor5-markdown-gfm/src/markdown",
    mediaEmbed: "@ckeditor/ckeditor5-media-embed/src/mediaembed",
    mention: "@ckeditor/ckeditor5-mention/src/mention",
    pageBreak: "@ckeditor/ckeditor5-page-break/src/pagebreak",
    paragraph: "@ckeditor/ckeditor5-paragraph/src/paragraph",
    pasteFromOffice: "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice",
    removeFormat: "@ckeditor/ckeditor5-remove-format/src/removeformat",
    restrictedEditing: "@ckeditor/ckeditor5-restricted-editing/src/restrictedediting",
    selectAll: "@ckeditor/ckeditor5-select-all/src/selectall",
    showBlocks: "@ckeditor/ckeditor5-show-blocks/src/showblocks",
    sourceEditing: "@ckeditor/ckeditor5-source-editing/src/sourceediting",
    specialCharacters: "@ckeditor/ckeditor5-special-characters/src/specialcharacters",
    strikethrough: "@ckeditor/ckeditor5-basic-styles/src/strikethrough",
    subscript: "@ckeditor/ckeditor5-basic-styles/src/subscript",
    superscript: "@ckeditor/ckeditor5-basic-styles/src/superscript",
    style: "@ckeditor/ckeditor5-style/src/style",
    table: "@ckeditor/ckeditor5-table/src/table",
    tableToolbar: "@ckeditor/ckeditor5-table/src/tabletoolbar",
    tableProperties: "@ckeditor/ckeditor5-table/src/tableProperties",
    tableCellProperties: "@ckeditor/ckeditor5-language/src/tablecellproperties",
    tableColumnResize: "@ckeditor/ckeditor5-language/src/tablecolumnresize",
    tableCaption: "@ckeditor/ckeditor5-language/src/tablecaption",
    textPartLanguage: "@ckeditor/ckeditor5-language/src/textpartlanguage",
    textPartLanguageEditing: "@ckeditor/ckeditor5-language/src/textpartlanguageediting",
    todoList: "@ckeditor/ckeditor5-list/src/todolist",
    underline: "@ckeditor/ckeditor5-basic-styles/src/underline",
    wordCount: "@ckeditor/ckeditor5-word-count/src/wordcount",

    // Premium Features
    multiLevelList: "@ckeditor/ckeditor5-list/src/multilevellist",
    aiCommands: "@ckeditor/ckeditor5-ai/src/aicommands",
    aiAssistant: "@ckeditor/ckeditor5-ai/src/aiassistant",
    exportPdfPremium: "@ckeditor/ckeditor5-export-pdf/src/exportpdf",
    exportWordPremium: "@ckeditor/ckeditor5-export-word/src/exportword",
    caseChange: "@ckeditor/ckeditor5-case-change/src/casechange",
    insertTemplate: "@ckeditor/ckeditor5-template/src/inserttemplate",
    tableOfContents: "@ckeditor/ckeditor5-toc/src/tableofcontents",
    formatPainter: "@ckeditor/ckeditor5-format-painter/src/formatpainter",
    importWord: "@ckeditor/ckeditor5-import-word/src/importword"
};

const loadedPlugins: Map<string, boolean> = new Map();

async function CustomUploadAdapterPlugin(editor: any) {
    editor.plugins.get("FileRepository").createUploadAdapter = (loader: any) => {
        return new CustomUploadAdapter(loader, "/ckeditor/upload");
    };
}

const loadPlugins = async (items: any[], plugins: any[]) => {
    const importPlugin = async (key: string) => {
        if (!loadedPlugins.has(key)) {
            loadedPlugins.set(key, true);
            const importedPlugin = await import(pluginImports[key]);
            plugins.push(importedPlugin.default);
        }
    };

    const processItems = async (items: any[]) => {
        for (const item of items) {
            if (typeof item === "object" && item.items) {
                await processItems(item.items);
                continue;
            }

            switch (item) {
                case "alignment:left":
                case "alignment:right":
                case "alignment:center":
                case "alignment:justify":
                case "alignment":
                    await importPlugin("alignment");
                    break;
                case "outdent":
                case "indent":
                    await importPlugin("indent");
                    await importPlugin("indentBlock");
                    break;
                case "unlink":
                case "link":
                    await importPlugin("link");
                    await importPlugin("autolink");
                    break;
                case "numberedList":
                case "bulletedList":
                case "list":
                    await importPlugin("list");
                    await importPlugin("listProperties");
                    break;
                case "uploadImage":
                    await importPlugin("image");
                    await importPlugin("imageEditing");
                    await importPlugin("imageBlock");
                    await importPlugin("imageCaption");
                    await importPlugin("imageStyle");
                    await importPlugin("imageToolbar");
                    await importPlugin("imageResize");
                    await importPlugin("imageUpload");
                    await importPlugin("cloudServices");
                    plugins.push(CustomUploadAdapterPlugin);
                    break;
                case "htmlEmbed":
                    await importPlugin("generalHtmlSupport");
                    await importPlugin("htmlEmbed");
                    break;
                case "style":
                    if (!loadedPlugins.has("generalHtmlSupport")) {
                        await importPlugin("generalHtmlSupport");
                    }
                    await importPlugin("style");
                    break;
                case "insertTable":
                case "tableColumn":
                case "tableRow":
                case "table":
                    await importPlugin("table");
                    await importPlugin("tableToolbar");
                    await importPlugin("tableProperties");
                    await importPlugin("tableCellProperties");
                    await importPlugin("tableColumnResize");
                    await importPlugin("tableCaption");
                    break;
                case "sourceEditing":
                    await importPlugin("fullPage");
                    if (!loadedPlugins.has("generalHtmlSupport")) {
                        await importPlugin("generalHtmlSupport");
                    }
                    await importPlugin("sourceEditing");
                    break;
                case "textPartLanguage":
                    await importPlugin("textPartLanguage");
                    await importPlugin("textPartLanguageEditing");
                    break;
                case "multiLevelList":
                    await importPlugin("list");
                    await importPlugin("multiLevelList");
                    break;
                case "exportPdf":
                    await importPlugin("exportPdf");
                    break;
                case "exportWord":
                    await importPlugin("exportWord");
                    break;
                default:
                    await importPlugin(item);
                    break;
            }
        }
    };

    await processItems(items);
};

export { loadPlugins, pluginImports };
