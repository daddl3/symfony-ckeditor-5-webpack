export class CustomUploadAdapter {
    loader: any;
    uploadUrl: string;
    xhr: XMLHttpRequest | null;

    constructor(loader: any, uploadUrl: string) {
        this.loader = loader;
        this.uploadUrl = uploadUrl;
        this.xhr = null;
    }

    upload(): Promise<{ default: string }> {
        return this.loader.file.then(
            (file: File) =>
                new Promise<{ default: string }>((resolve, reject) => {
                    this._initRequest();
                    this._initListeners(resolve, reject, file);
                    this._sendRequest(file);
                })
        );
    }

    abort(): void {
        if (this.xhr) {
            this.xhr.abort();
        }
    }

    _initRequest(): void {
        const xhr = (this.xhr = new XMLHttpRequest());
        xhr.open("POST", this.uploadUrl, true);
        xhr.responseType = "json";
    }

    _initListeners(
        resolve: (value: { default: string }) => void,
        reject: (reason?: any) => void,
        file: File
    ): void {
        const xhr = this.xhr!;
        const loader = this.loader;
        const genericErrorText = `Couldn't upload file: ${file.name}.`;

        xhr.addEventListener("error", () => reject(genericErrorText));
        xhr.addEventListener("abort", () => reject());
        xhr.addEventListener("load", () => {
            const response = xhr.response;
            if (response.error) {
                return reject(
                    response && response.error && response.error.message
                        ? response.error.message
                        : genericErrorText
                );
            }
            resolve({
                default: response.url
            });
        });

        if (xhr.upload) {
            xhr.upload.addEventListener("progress", (evt: ProgressEvent) => {
                if (evt.lengthComputable) {
                    loader.uploadTotal = evt.total;
                    loader.uploaded = evt.loaded;
                }
            });
        }
    }

    _sendRequest(file: File): void {
        const data = new FormData();
        data.append("upload", file);

        this.xhr!.send(data as XMLHttpRequestBodyInit);
    }
}
