// Async Imports
let defaultConfig: any;
let loadEditor: any;

const loadDependencies = async () => {
    await import("../css/styles.css");

    const configModule = await import("./partials/defaultConfig.js");
    defaultConfig = configModule.default;

    const editorLoaderModule = await import("./partials/editorLoader.js");
    loadEditor = editorLoaderModule.loadEditor;
};

// Global Declaration
declare global {
    interface Window {
        defaultConfig: any;
        editors: any;
    }
}

// Set of Initialized Textareas
const initializedTextareas = new Set<string>();

// Mutation Observer to Initialize Editors
const observer = new MutationObserver(async () => {
    if ((observer as any).debounceTimeout) {
        clearTimeout((observer as any).debounceTimeout);
    }

    (observer as any).debounceTimeout = setTimeout(async () => {
        let textareas = Array.from(
            document.querySelectorAll(
                "textarea[data-ckeditor5-config]:not(.ckeditor-initialized)"
            )
        );

        for (const textarea of textareas) {
            const textareaElement = textarea as HTMLTextAreaElement;
            if (!initializedTextareas.has(textareaElement.id)) {
                initializedTextareas.add(textareaElement.id);
                await initializeEditor(textareaElement);
            }
        }
    }, 10);
});

// Object to Store Editors
const editors: { [key: string]: any } = {};

// Function to Initialize an Editor
async function initializeEditor(textarea: HTMLTextAreaElement) {
    const configParse = JSON.parse(textarea.getAttribute("data-ckeditor5-config")!);
    const config = configParse !== "default" ? configParse : window.defaultConfig;

    await loadEditor(config)
        .then((ClassicEditor: any) => {
            if (!textarea.classList.contains("ckeditor-initialized")) {
                ClassicEditor.create(textarea, config)
                    .then((editor: any) => {
                        textarea.classList.add("ckeditor-initialized");
                        editor.model.document.on("change:data", () => {
                            textarea.value = editor.getData();
                        });
                        editors[textarea.id] = editor;
                        console.log(`Editor initialized for #${textarea.id}`);
                    })
                    .catch((error: any) => {
                        console.error(
                            `Error initializing editor for #${textarea.id}: ${error}`
                        );
                    });
            }
        })
        .catch((error: any) => {
            console.error(`Error loading editor: ${error}`);
        });
}

// Attach Editors to Window Object
window.editors = editors;

// Start Observer
observer.observe(document.body, {
    childList: true,
    subtree: true
});

// Load Dependencies and Initialize Default Config
loadDependencies().then(() => {
    window.defaultConfig = defaultConfig;
});
