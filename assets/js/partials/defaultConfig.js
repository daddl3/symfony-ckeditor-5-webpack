"use strict";

export const defaultConfig = {
    toolbar: {
        items: [
            "accessibilityHelp",
            "heading",
            "|",
            "bold",
            "italic",
            "underline",
            "link",
            "|",
            "bulletedList",
            "numberedList",
            "todoList",
            "|",
            "alignment",
            "|",
            "code",
            "|",
            "blockQuote",
            "uploadImage",
            "insertTable",
            "|",
            "sourceEditing"
        ],
        shouldNotGroupWhenFull: false
    },
    language: "en",
    table: {
        contentToolbar: ["tableColumn", "tableRow", "mergeTableCells"]
    },
    image: {
        resizeOptions: [
            {
                name: "resizeImage:original",
                value: null,
                label: "Original"
            },
            {
                name: "resizeImage:100",
                value: "100",
                label: "100%"
            },
            {
                name: "resizeImage:60",
                value: "60",
                label: "60%"
            },
            {
                name: "resizeImage:50",
                value: "50",
                label: "50%"
            },
            {
                name: "resizeImage:40",
                value: "40",
                label: "40%"
            },
            {
                name: "resizeImage:30",
                value: "30",
                label: "30%"
            },
            {
                name: "resizeImage:20",
                value: "20",
                label: "20%"
            }
        ],
        toolbar: [
            "linkImage",
            "|",
            "imageStyle:inline",
            "imageStyle:wrapText",
            "imageStyle:breakText",
            "|",
            "resizeImage",
            "|",
            "toggleImageCaption",
            "imageTextAlternative"
        ]
    },
    htmlSupport: {
        allow: [
            {
                name: /^.*$/,
                attributes: true,
                classes: true,
                styles: true
            }
        ]
    }
};
