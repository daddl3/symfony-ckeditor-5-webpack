"use strict";

function CustomUploadAdapterPlugin(editor) {
    // Load the custom upload adapter asynchronously
    (async () => {
        const { CustomUploadAdapter } = await import("../custom-upload-adapter");

        editor.plugins.get("FileRepository").createUploadAdapter = (loader) => {
            return new CustomUploadAdapter(loader, "/ckeditor/upload");
        };
    })();
}

export async function loadPlugins(items, plugins, license, extra) {
    const importPlugin = async (key) => {
        switch (key) {
            case "accessibilityHelp":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-ui/src/editorui/accessibilityhelp/accessibilityhelp"
                        )
                    ).default
                );
                break;
            case "alignment":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-alignment/src/alignment")).default
                );
                break;
            case "autoformat":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-autoformat/src/autoformat"))
                        .default
                );
                break;
            case "autolink":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-link/src/autolink")).default
                );
                break;
            case "blockQuote":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-block-quote/src/blockquote"))
                        .default
                );
                break;
            case "bold":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/bold")).default
                );
                break;
            case "cloudServices":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-cloud-services/src/cloudservices"))
                        .default
                );
                break;
            case "code":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/code")).default
                );
                break;
            case "codeBlock":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-code-block/src/codeblock")).default
                );
                break;
            case "essentials":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-essentials/src/essentials"))
                        .default
                );
                break;
            case "exportPdf":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-export-pdf/src/exportpdf")).default
                );
                break;
            case "exportWord":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-export-word/src/exportword"))
                        .default
                );
                break;
            case "findAndReplace":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-find-and-replace/src/findandreplace"
                        )
                    ).default
                );
                break;
            case "fontBackgroundColor":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-font/src/fontbackgroundcolor"))
                        .default
                );
                break;
            case "fontColor":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-font/src/fontcolor")).default
                );
                break;
            case "fontFamily":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-font/src/fontfamily")).default
                );
                break;
            case "fontSize":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-font/src/fontsize")).default
                );
                break;
            case "generalHtmlSupport":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-html-support/src/generalhtmlsupport"
                        )
                    ).default
                );
                break;
            case "fullPage":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-html-support/src/fullpage"))
                        .default
                );
                break;
            case "heading":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-heading/src/heading")).default
                );
                break;
            case "highlight":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-highlight/src/highlight")).default
                );
                break;
            case "horizontalLine":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-horizontal-line/src/horizontalline"
                        )
                    ).default
                );
                break;
            case "htmlcomment":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-html-support/src/htmlcomment"))
                        .default
                );
                break;
            case "htmlEmbed":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-html-embed/src/htmlembed")).default
                );
                break;
            case "image":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/image")).default
                );
                break;
            case "imageCaption":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imagecaption")).default
                );
                break;
            case "imageEditing":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/image/imageediting"))
                        .default
                );
                break;
            case "imageBlock":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imageblock")).default
                );
                break;
            case "imageinsertui":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-image/src/imageinsert/imageinsertui"
                        )
                    ).default
                );
                break;
            case "imageResize":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imageresize")).default
                );
                break;
            case "insertImage":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imageinsert")).default
                );
                break;
            case "insertImageViaUrl":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imageinsertviaurl"))
                        .default
                );
                break;
            case "imageStyle":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imagestyle")).default
                );
                break;
            case "imageToolbar":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imagetoolbar")).default
                );
                break;
            case "imageUpload":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/imageupload")).default
                );
                break;
            case "autoimage":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/autoimage")).default
                );
                break;
            case "indent":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-indent/src/indent")).default
                );
                break;
            case "indentBlock":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-indent/src/indentblock")).default
                );
                break;
            case "italic":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/italic")).default
                );
                break;
            case "link":
                plugins.push((await import("@ckeditor/ckeditor5-link/src/link")).default);
                break;
            case "list":
                plugins.push((await import("@ckeditor/ckeditor5-list/src/list")).default);
                break;
            case "listProperties":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-list/src/listproperties")).default
                );
                break;
            case "markdown":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-markdown-gfm/src/markdown"))
                        .default
                );
                break;
            case "mediaEmbed":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-media-embed/src/mediaembed"))
                        .default
                );
                break;
            case "mention":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-mention/src/mention")).default
                );
                break;
            case "pageBreak":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-page-break/src/pagebreak")).default
                );
                break;
            case "paragraph":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-paragraph/src/paragraph")).default
                );
                break;
            case "pasteFromOffice":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-paste-from-office/src/pastefromoffice"
                        )
                    ).default
                );
                break;
            case "removeFormat":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-remove-format/src/removeformat"))
                        .default
                );
                break;
            case "restrictedEditingException":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-restricted-editing/src/standardeditingmode"
                        )
                    ).default
                );
                break;
            case "restrictedEditing":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-restricted-editing/src/restrictededitingmode"
                        )
                    ).default
                );
                break;
            case "selectAll":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-select-all/src/selectall")).default
                );
                break;
            case "showBlocks":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-show-blocks/src/showblocks"))
                        .default
                );
                break;
            case "sourceEditing":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-source-editing/src/sourceediting"))
                        .default
                );
                break;
            case "specialCharacters":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-special-characters/src/specialcharacters"
                        )
                    ).default
                );
                break;
            case "specialCharactersEssentials":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-special-characters/src/specialcharactersessentials"
                        )
                    ).default
                );
                break;
            case "strikethrough":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/strikethrough"))
                        .default
                );
                break;
            case "subscript":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/subscript"))
                        .default
                );
                break;
            case "superscript":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/superscript"))
                        .default
                );
                break;
            case "style":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-style/src/style")).default
                );
                break;
            case "pasteFromMarkdownExperimental":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-markdown-gfm/src/pastefrommarkdownexperimental"
                        )
                    ).default
                );
                break;
            case "table":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/table")).default
                );
                break;
            case "tableToolbar":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/tabletoolbar")).default
                );
                break;
            case "tableProperties":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/tableproperties"))
                        .default
                );
                break;
            case "tableCellProperties":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/tablecellproperties"))
                        .default
                );
                break;
            case "tableColumnResize":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/tablecolumnresize"))
                        .default
                );
                break;
            case "tableCaption":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-table/src/tablecaption")).default
                );
                break;
            case "textPartLanguage":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-language/src/textpartlanguage"))
                        .default
                );
                break;
            case "textPartLanguageEditing":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-language/src/textpartlanguageediting"
                        )
                    ).default
                );
                break;
            case "texttransformation":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-typing/src/texttransformation"))
                        .default
                );
                break;
            case "todoList":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-list/src/todolist")).default
                );
                break;
            case "underline":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-basic-styles/src/underline"))
                        .default
                );
                break;
            case "exportPdfPremium":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-export-pdf/src/exportpdf")).default
                );
                break;
            case "exportWordPremium":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-export-word/src/exportword"))
                        .default
                );
                break;
            case "importWord":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-import-word/src/importword"))
                        .default
                );
                break;
            case "pastefromofficeenhanced":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-paste-from-office-enhanced/src/pastefromofficeenhanced"
                        )
                    ).default
                );
                break;
            default:
                break;
        }
    };

    const importPremiumPlugin = async (key) => {
        switch (key) {
            case "multiLevelList":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-list-multi-level/src/multilevellist"
                        )
                    ).default
                );
                break;
            case "aiAssistant":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-ai/src/aiassistant")).default
                );
                break;
            case "openAITextAdapter ":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-ai/src/adapters/aiadapter"))
                        .default
                );
                break;
            case "insertTemplate":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-template/src/template")).default
                );
                break;
            case "formatPainter":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-format-painter/src/formatpainter"))
                        .default
                );
                break;
            case "caseChange":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-case-change/src/casechange"))
                        .default
                );
                break;
            case "tableOfContents":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-document-outline/src/tableofcontents"
                        )
                    ).default
                );
                break;
            case "PictureEditing":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-image/src/pictureediting")).default
                );
                break;
            default:
                break;
        }
    };

    const processItems = async (items) => {
        for (const item of items) {
            if (typeof item === "object" && item.items) {
                await processItems(item.items);
                continue;
            }
            switch (item) {
                case "alignment:left":
                case "alignment:right":
                case "alignment:center":
                case "alignment:justify":
                case "alignment":
                    await importPlugin("alignment");
                    break;
                case "outdent":
                case "indent":
                    await importPlugin("indent");
                    await importPlugin("indentBlock");
                    break;
                case "undo":
                case "redo":
                case "|":
                    break;
                case "unlink":
                case "link":
                    await importPlugin("link");
                    await importPlugin("autolink");
                    break;
                case "numberedList":
                case "bulletedList":
                case "list":
                    await importPlugin("list");
                    await importPlugin("listProperties");
                    break;
                case "uploadImage": // todo issue with insertImageui
                    await importPlugin("image");
                    await importPlugin("imageEditing");
                    await importPlugin("imageBlock");
                    await importPlugin("imageinsertui");
                    await importPlugin("imageCaption");
                    await importPlugin("imageStyle");
                    await importPlugin("imageToolbar");
                    await importPlugin("imageResize");
                    await importPlugin("imageUpload");
                    await importPlugin("cloudServices");
                    plugins.push(CustomUploadAdapterPlugin);
                    break;
                case "insertImage":
                    await importPlugin("image");
                    await importPlugin("imageEditing");
                    await importPlugin("imageBlock");
                    await importPlugin("imageCaption");
                    await importPlugin("imageStyle");
                    await importPlugin("imageToolbar");
                    await importPlugin("imageResize");
                    await importPlugin("insertImage");
                    plugins.push(CustomUploadAdapterPlugin);
                    break;
                case "htmlEmbed":
                    await importPlugin("generalHtmlSupport");
                    await importPlugin("htmlEmbed");
                    break;
                case "style":
                    await importPlugin("generalHtmlSupport");
                    await importPlugin("style");
                    break;
                case "insertTable":
                case "tableColumn":
                case "tableRow":
                case "table":
                    await importPlugin("table");
                    await importPlugin("tableToolbar");
                    await importPlugin("tableProperties");
                    await importPlugin("tableCellProperties");
                    await importPlugin("tableColumnResize");
                    await importPlugin("tableCaption");
                    break;
                case "sourceEditing":
                    await importPlugin("fullPage");
                    await importPlugin("generalHtmlSupport");
                    await importPlugin("sourceEditing");
                    break;
                case "textPartLanguage":
                    await importPlugin("textPartLanguage");
                    await importPlugin("textPartLanguageEditing");
                    break;
                case "exportPdf":
                    await importPlugin("exportPdf");
                    await importPlugin("cloudServices");
                    break;
                case "importWord":
                    await importPlugin("importWord");
                    await importPlugin("cloudServices");
                    break;
                case "exportWord":
                    await importPlugin("exportWord");
                    await importPlugin("cloudServices");
                    break;
                case "specialCharacters":
                    await importPlugin("specialCharacters");
                    await importPlugin("specialCharactersEssentials");
                    break;
                default:
                    await importPlugin(item);
                    break;
            }
        }
    };

    const processPremiumItems = async (items) => {
        for (const item of items) {
            if (typeof item === "object" && item.items) {
                await processPremiumItems(item.items);
                continue;
            }
            switch (item) {
                case "aiAssistant":
                case "aiCommands":
                    await importPlugin("aiAssistant");
                    await importPlugin("openAITextAdapter ");
                    break;
                case "tableOfContents":
                    await importPremiumPlugin("tableOfContents");
                    await importPlugin("heading");
                    break;
                case "uploadImage":
                    await importPremiumPlugin("PictureEditing");
                    break;
                case "autoimage":
                    await importPremiumPlugin("PictureEditing");
                    break;
                case "insertImage":
                    await importPremiumPlugin("PictureEditing");
                    break;
                case "multiLevelList":
                    await importPlugin("list");
                    await importPremiumPlugin("multiLevelList");
                    break;
                default:
                    await importPremiumPlugin(item);
                    break;
            }
        }
    };

    const processPremiumItemsFromExtraConfig = async (items) => {
        for (const item of items) {
            switch (item) {
                case "autoimage":
                    plugins.push(CustomUploadAdapterPlugin);
                    await importPlugin("autoimage");
                    await importPlugin("insertImage");
                    await importPlugin("insertImageViaUrl");
                    await importPlugin("image");
                    break;
                case "pasteFromOfficeEnhanced":
                    await importPlugin("pasteFromOffice");
                    await importPremiumPlugin("pastefromofficeenhanced");
                    await importPlugin("generalHtmlSupport");
                    await importPlugin("alignment");
                    await importPlugin("bold");
                    await importPlugin("italic");
                    await importPlugin("underline");
                    await importPlugin("strikethrough");
                    await importPlugin("subscript");
                    await importPlugin("superscript");
                    await importPlugin("fontColor");
                    await importPlugin("fontBackgroundColor");
                    await importPlugin("fontFamily");
                    await importPlugin("fontSize");
                    await importPlugin("indent");
                    await importPlugin("indentBlock");
                    await importPlugin("table");
                    await importPlugin("tableToolbar");
                    await importPlugin("tableProperties");
                    await importPlugin("tableCellProperties");
                    await importPlugin("tableColumnResize");
                    await importPlugin("tableCaption");
                    break;

                default:
                    await importPlugin(item);
                    break;
            }
        }
    };

    if (!license && !extra) {
        await processItems(items);
    }

    if (license) {
        await processPremiumItems(items);
    }

    if (extra) {
        await processPremiumItemsFromExtraConfig(items);
    }
}

export async function loadPluginsFromConfig(config) {
    const plugins = config.plugins;
    const importPlugin = async (key) => {
        switch (key) {
            case "mention":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-mention/src/mention")).default
                );
                break;
            case "mergeFields":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-merge-fields/src/mergefields"))
                        .default
                );
                break;
            case "slashCommand":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-slash-command/src/slashcommand"))
                        .default
                );
                break;
            case "wordCount":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-word-count/src/wordcount")).default
                );
                break;
            case "autosave":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-autosave/src/autosave")).default
                );
                break;
            case "minimap":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-minimap/src/minimap")).default
                );
                break;
            case "documentOutline":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-document-outline/src/documentoutline"
                        )
                    ).default
                );
                break;
            case "title":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-heading/src/title")).default
                );
                break;
            case "clipboard":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-clipboard/src/clipboard")).default
                );
                break;
            case "pagination":
                plugins.push(
                    (await import("@ckeditor/ckeditor5-pagination/src/pagination"))
                        .default
                );
                break;
            case "balloonToolbar":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-ui/src/toolbar/balloon/balloontoolbar"
                        )
                    ).default
                );
                break;
            case "blockToolbar":
                plugins.push(
                    (
                        await import(
                            "@ckeditor/ckeditor5-ui/src/toolbar/block/blocktoolbar"
                        )
                    ).default
                );
                break;
            default:
                break;
        }
    };

    const processItems = async (items) => {
        for (const [key] of Object.entries(items)) {
            switch (key) {
                case "mention":
                    await importPlugin("mention");
                    break;
                case "balloonToolbar":
                    await importPlugin("balloonToolbar");
                    break;
                case "blockToolbar":
                    await importPlugin("blockToolbar");
                    break;
                case "mergeFields":
                    await importPlugin("mention");
                    await importPlugin("mergeFields");
                    break;
                case "slashCommand":
                    await importPlugin("mention");
                    await importPlugin("slashCommand");
                    break;
                case "wordCount":
                    await importPlugin("wordCount");
                    break;
                case "autosave":
                    await importPlugin("autosave");
                    break;
                case "minimap":
                    await importPlugin("minimap");
                    break;
                case "documentOutline":
                    await importPlugin("documentOutline");
                    await importPlugin("heading");
                    break;
                case "title":
                    await importPlugin("title");
                    break;
                case "clipboard":
                    await importPlugin("clipboard");
                    break;
                case "pagination":
                    await importPlugin("pagination");
                    break;
                default:
                    break;
            }
        }
    };

    await processItems(config);
}
