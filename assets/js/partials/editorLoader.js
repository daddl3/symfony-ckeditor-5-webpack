"use strict";

export let loadEditor;

let pushTranslation;
let pushTranslationPremium;
let loadPlugins;
let loadPluginsFromConfig;
let editor;

loadEditor = async (config, textareaName, color) => {
    // Begin loading process log
    await console.groupCollapsed(
        `%c[Editor Loading] "${textareaName}"`,
        `color: ${color}; font-weight: bold;`
    );

    await console.log(
        `%c⬇️ Starting to load editor for textarea: "${textareaName}" with config:`,
        `color: ${color};`,
        config
    );

    // Import translations module
    try {
        const translationsModule = await import("./translations.js");
        pushTranslation = translationsModule.pushTranslation;
        pushTranslationPremium = translationsModule.pushTranslationPremium;
        await console.log(
            `%c✔️ Successfully loaded translations module.`,
            `color: ${color};`
        );
    } catch (error) {
        await console.error(
            `%c❌ Error loading translations module: ${error}`,
            `color: red; font-weight: bold;`
        );
    }

    // Import plugins module
    try {
        const loadPluginsModule = await import("./loadPlugins.js");
        loadPlugins = loadPluginsModule.loadPlugins;
        loadPluginsFromConfig = loadPluginsModule.loadPluginsFromConfig;
        await console.log(`%c✔️ Successfully loaded plugins module.`, `color: ${color};`);
    } catch (error) {
        await console.error(
            `%c❌ Error loading plugins module: ${error}`,
            `color: red; font-weight: bold;`
        );
    }

    const editorConfig = config.editor ?? "Classic";

    await console.log(
        `%c⏳ ${editorConfig} Editor not loaded yet, loading now...`,
        `color: ${color};`
    );
    try {
        editor = await (async () => {
            switch (editorConfig) {
                case "Decoupled":
                    return (
                        await import("@ckeditor/ckeditor5-editor-decoupled/src/index")
                    ).DecoupledEditor;
                case "Balloon":
                    return (await import("@ckeditor/ckeditor5-editor-balloon/src/index"))
                        .BalloonEditor;
                case "Inline":
                    return (await import("@ckeditor/ckeditor5-editor-inline/src/index"))
                        .InlineEditor;
                case "Multi":
                    return (
                        await import("@ckeditor/ckeditor5-editor-multi-root/src/index")
                    ).MultiRootEditor;
                default:
                    return (await import("@ckeditor/ckeditor5-editor-classic/src/index"))
                        .ClassicEditor;
            }
        })();
    } catch (error) {
        await console.error(
            `%c❌ Error loading ${editorConfig} Editor: ${error}`,
            `color: red; font-weight: bold;`
        );
    }

    await console.log(
        `%c✔️ ${editorConfig} Editor loaded successfully.`,
        `color: ${color};`
    );

    // Create the editor instance
    try {
        const editorInstance = await editor.create(
            document.querySelector(`#${textareaName}`),
            config
        );
        console.log("Editor created successfully:", editorInstance);
    } catch (error) {
        console.error("Error during editor creation:", error);
    }

    // Rest of the loading process (translations, plugins, etc.)
    await loadEditorResources(config, textareaName, color);

    return editor;
};

// Loading additional resources like translations and plugins
async function loadEditorResources(config, textareaName, color) {
    // Loading translations for Editor
    await console.groupCollapsed(
        `%c[Editor: "${textareaName}"] Translations`,
        `color: ${color};`
    );
    try {
        config.translations = [];
        const language = config.language;
        if (typeof language === "string") {
            const trans = await pushTranslation(language);
            config.translations.push(trans.default);
            await console.log(
                `%c✔️ Loaded translation for language: "${language}"`,
                `color: ${color};`
            );
        } else if (typeof language === "object") {
            if (language.ui) {
                const trans = await pushTranslation(language.ui);
                config.translations.push(trans.default);
                await console.log(
                    `%c✔️ Loaded UI translation for language: "${language.ui}"`,
                    `color: ${color};`
                );
            }
            if (language.content) {
                const trans = await pushTranslation(language.content);
                config.translations.push(trans.default);
                await console.log(
                    `%c✔️ Loaded content translation for language: "${language.content}"`,
                    `color: ${color};`
                );
            }
        }
    } catch (error) {
        await console.error(
            `%c❌ Error loading translations: ${error}`,
            `color: red; font-weight: bold;`
        );
    }
    await console.groupEnd(); // End translations group

    // Loading editor plugins
    await console.groupCollapsed(
        `%c[Editor: "${textareaName}"] Plugins`,
        `color: ${color};`
    );
    try {
        config.plugins = [];
        const toolbarItems = config.toolbar.items;

        await console.log(
            `%c⬇️ Loading default and toolbar plugins:`,
            `color: ${color};`,
            toolbarItems
        );

        // Load default plugins
        await loadPlugins(
            ["essentials", "autoformat", "texttransformation", "paragraph"],
            config.plugins
        );

        // Load toolbar plugins
        await loadPlugins(toolbarItems, config.plugins);

        // Load premium plugins if license key is found
        if (config.licenseKey !== undefined) {
            await console.log(
                `%c🔑 License key found, loading premium plugins...`,
                `color: ${color};`
            );
            await loadPlugins(toolbarItems, config.plugins, true);
            const language = config.language;
            if (typeof language === "string") {
                const trans = await pushTranslationPremium(language);
                config.translations.push(trans.default);
            } else if (typeof language === "object") {
                if (language.ui) {
                    const trans = await pushTranslationPremium(language.ui);
                    config.translations.push(trans.default);
                }
                if (language.content) {
                    const trans = await pushTranslationPremium(language.content);
                    config.translations.push(trans.default);
                }
            }
        }
    } catch (error) {
        await console.error(
            `%c❌ Error loading plugins: ${error}`,
            `color: red; font-weight: bold;`
        );
    }
    await console.groupEnd(); // End plugins group

    // Load any additional plugins from the configuration
    await console.groupCollapsed(
        `%c[Editor: "${textareaName}"] Additional Plugins`,
        `color: ${color};`
    );
    try {
        await loadPluginsFromConfig(config);
        if (config.extraPlugins) {
            await console.log(
                `%c⬇️ Loading extra plugins: ${config.extraPlugins}`,
                `color: ${color};`
            );
            await loadPlugins(config.extraPlugins, config.plugins, false, true);
            delete config.extraPlugins;
        }
    } catch (error) {
        await console.error(
            `%c❌ Error loading additional plugins: ${error}`,
            `color: red; font-weight: bold;`
        );
    }
    await console.groupEnd(); // End additional plugins group

    // Remove duplicate translations
    config.translations = uniqueArray(config.translations);
    await console.log(`%c✔️ Removed duplicate translations.`, `color: ${color};`);

    await console.log(
        `%c✔️ Editor for "${textareaName}" is fully configured and ready.`,
        `color: ${color};`
    );

    await console.groupEnd(); // End main group

    return editor;
}

// Helper function to remove duplicate translations
function uniqueArray(arr) {
    const uniqueSet = new Set(arr);
    return [...uniqueSet];
}
