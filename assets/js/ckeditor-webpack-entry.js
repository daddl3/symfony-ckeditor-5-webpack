// ckeditor.config.js
const { styles } = require("@ckeditor/ckeditor5-dev-utils");

function addCKEditor(Encore, enableSourceMap = false) {
    Encore.addLoader({
        test: /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/,
        loader: "raw-loader"
    })
        .configureLoaderRule("images", (loader) => {
            loader.exclude = /ckeditor5-[^/\\]+[/\\]theme[/\\]icons[/\\][^/\\]+\.svg$/;
        })
        .addLoader({
            test: /ckeditor5-[^/\\]+[/\\]theme[/\\].+\.css$/,
            loader: "postcss-loader",
            options: {
                postcssOptions: styles.getPostCssConfig({
                    themeImporter: {
                        themePath: require.resolve("@ckeditor/ckeditor5-theme-lark")
                    },
                    minify: true
                }),
                sourceMap: enableSourceMap // Enable sourceMap based on the boolean flag
            }
        });
}

module.exports = addCKEditor;
